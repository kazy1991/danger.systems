require 'middleman-gh-pages'
require 'json'
require 'bundler'

js_tasks = %w[setup_js setup_js_logo setup_monaco]
ruby_tasks = %w[plugins_core plugins_external grab_dangerfiles search_plugin_json getting_started_docs]
desc 'Generates all the static resources necessary to run the site.'
task generate: (js_tasks + ruby_tasks).map { |task| 'generator:' + task } do
  # Your Gemfile.lock tends to get put out of sync after some of the commands.
  puts 'Shippit'
end

desc 'Things that generate JSON'
namespace :generator do
  desc 'Generates a JSON file that represents the core plugins documentation'
  task :plugins_core do
    # Make sure we have a folder for json_data, it's not in git.
    Dir.mkdir('static/json_data') unless Dir.exist?('static/json_data')

    # Grab the Danger gem, pull out the file paths for the core plugins
    danger_gem = Gem::Specification.find_by_name 'danger'
    danger_core_plugins = Dir.glob(danger_gem.gem_dir + '/lib/danger/danger_core/plugins/*.rb')

    # Document them, move them into a nice JSON file
    output = `bundle exec danger plugins json #{danger_core_plugins.join(' ')}`
    abort('Could not generate the core plugin metadata') if output.empty?
    File.write('static/json_data/core.json', output)
    puts 'Generated core API metadata'
  end

  desc 'Generates a JSON file that represents the external plugins documentation'
  task :plugins_external do
    plugins = JSON.parse(File.read('plugins.json'))
    # plugins = ["danger-xcodebuild"]
    plugin_objects = []

    plugins.each do |plugin|
      puts "Generating docs for #{plugin}"
      # Generate the Website's plugin doc, by passing in the gem names
      plugin_json = `bundle exec danger plugins json #{plugin}`
      next unless $?.success?
      plugin_objects << JSON.parse(plugin_json)
    end

    File.write('static/json_data/plugins.json', plugin_objects.flatten.to_json)
    puts 'Generated plugin metadata'

    plugins_file_path = File.join File.dirname(__FILE__), 'plugins-search-generated.json'
    gem_metadata = plugin_objects.flatten.map { |p| p['gem_metadata'] }

    File.write(plugins_file_path, { plugins: gem_metadata }.to_json)
    puts 'Generated search metadata for `danger search`.'
  end

  desc 'Generates a JSON file that represents the external plugins documentation'
  task :grab_dangerfiles do
    # Grab our Dangerfile plugins list
    dangerfile_repos = JSON.parse(File.read('example_oss_dangerfiles.json'))
    dangerfile_repos.each do |repo|
      require 'open-uri'
      require 'pygments'
      branch = repo.include?('#') ? repo.split('#').last : 'master'
      repo = repo.split('#').first

      dangerfile = open("https://raw.githubusercontent.com/#{repo}/#{branch}/Dangerfile").read

      path = "static/source/dangerfiles/#{repo.tr('/', '_')}.html"
      html = Pygments.highlight(dangerfile, lexer: 'ruby', options: { encoding: 'utf-8' })

      File.write(path, html)
    end
    puts 'Downloaded Dangerfiles to `static/source/dangerfiles`'
  end

  desc 'Generate the website plugin search JSON file, this is different from the danger gem search - as one gem can have multiple plugins'
  task :search_plugin_json do
    plugins = JSON.parse(File.read('static/json_data/plugins.json'))
    plugin_search_metadata = plugins.map do |plugin|
      {
        name: plugin['name'],
        gem: plugin['gem'],
        body: plugin['body_md'],
        instance: plugin['instance_name'],
        tags: plugin['tags'],
        see: plugin['see']
      }
    end

    File.write('static/json_data/plugin_search.json', plugin_search_metadata.to_json)
    puts 'Generated search JSON for inline search'
  end

  desc 'Generate the getting started guides metadata from Danger'
  task :getting_started_docs do
    `bundle exec danger systems ci_docs > static/json_data/ci_docs.json`
    puts 'Generated getting started CI documentation'
  end

  desc 'Generate the JSON + Images necessary for the Danger JS logo to change per-deploy'
  task :setup_js_logo do
    require 'twitter'
    require 'open-uri'

    # This is for an account @123123wqeqweqqwe
    # so I'm not worried if people try break in.

    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = 'EYRutegHyV4G9jpv1a1QoI4lf'
      config.consumer_secret     = 'GQlYCvHyLQx8N0qbxZMzUKp7T9r4PoiqJS5RC2r5Y7aMKyEasG'
      config.access_token        = '3330338902-ismc4FmfWPK3tAjqgs9AtOk0ehR59fGqSDEGAUO'
      config.access_token_secret = 'NTtMdvqPknqUtnYPPtn2l4xnTH294qxqbgDheKtGscYzM'
    end

    conway = client.user('cloudyconway')
    tweet = conway.tweet
    last_image = tweet.media.first.media_url
    `curl -o static/source/images/js/conway_original.png #{last_image}`
    File.write('static/source/images/js/conway_tweet.json', tweet.attrs.to_json)

    oembed = open("https://publish.twitter.com/oembed?url=https://twitter.com/cloudyconway/status/#{tweet.id}").read
    File.write('static/source/images/js/conway_tweet_oembed.json', oembed)

    sh 'yarn install' unless Dir.exist? 'node_modules'
    sh 'yarn run palette'
  end

  desc 'Sets up the required bits for the inline monaco editor'
  task :setup_monaco do
    sh 'yarn install' unless Dir.exist? 'node_modules'
    sh 'mkdir static/source/javascripts/monaco' unless Dir.exist? 'static/source/javascripts/monaco'
    sh 'cp -r node_modules/monaco-editor/* static/source/javascripts/monaco/'
  end

  desc 'Generate the getting started guides metadata from Danger'
  task :setup_js do
    `rm -rf danger-js` if Dir.exist? 'danger-js'
    `git clone https://github.com/danger/danger-js`

    # TODO: Get the NPM version and use that, or the latest tag?

    # Generate the JSON representation of the DSL
    Dir.chdir('danger-js') do
      sh 'yarn install'
      sh 'yarn docs'
    end

    # Migrate the API reference JSON
    sh 'cp danger-js/docs/js_ref_dsl_docs.json static/json_data'

    # Migrate the docs into the static site generator
    sh('mkdir -p static/source/js/') unless Dir.exist? 'static/source/js/'
    sh 'cp -r danger-js/docs/* static/source/js/'
    sh 'rm -rf danger-js/docs/doc_generate/'

    puts 'Generated the JS side'
  end
end

desc 'Runs the site locally'
task :serve do
  puts 'Running locally at http://localhost:4567'
  sh 'open http://localhost:4567'
  Dir.chdir('static') do
    sh 'bundle exec middleman server'
  end
end
